function isTuneIn(tab) {
    return tab.url.substr(0, "http://tunein.com/".length) === "http://tunein.com/";
}


function findSound(tabId) {
    chrome.tabs.executeScript(tabId, {
        file: "parser.js"
    });
}

chrome.browserAction.onClicked.addListener(function (tab) {

    if (isTuneIn(tab)) {
        findSound(tab.id);
    } else {
        chrome.tabs.getAllInWindow(null, function (tabs) {
            var targetTabId;

            var hipHopTabIds = [];
            var count = 0;
            for (var i = 0; i < tabs.length; i++) {
                if (isTuneIn(tabs[i])) {
                    hipHopTabIds[count++] = i;
                    targetTabId = tabs[i].id;
                }
            }

            if (hipHopTabIds.length > 1) {
                for (var i = 0; i < hipHopTabIds.length; i++) {
                    if (tabs[hipHopTabIds[i]].audible) {
                        targetTabId = tabs[hipHopTabIds[i]].id;
                        break;
                    }
                }
            }
            findSound(targetTabId);
        });
    }
});

chrome.runtime.onMessage.addListener(function (fullTrackName) {
    var begin = "http://vk.com/search?c%5Bq%5D=";
    var separator = "%20";
    var end = "&c%5Bsection%5D=audio";
    var partsOfTheTrackName = fullTrackName.replace("(HotJam)", "").split(" ");

    var url = begin;
    for (var i = 0; i < partsOfTheTrackName.length; i++) {
        url += partsOfTheTrackName[i].replace("&", "%26") + separator;
    }
    url += end;

    chrome.tabs.create({
        url: url
    });
});